# TODO List API

Just small experiments :)

Please provide `.env` file following this format:

```sh
PORT=3000
DB_FILE=/Users/this/Projects/ts/todo-list-api/db.sqlite
SECRET_PHRASE='THIS IS SUPER SECRET PHRASE EVER'
DEV=1
```

## Development

```
$ npm i
$ npm run start:dev
```

### API Docs

https://documenter.getpostman.com/view/1100715/SzKQy15V?version=latest#1fd9e864-4a2c-4010-b78b-60f0891ddbc6
