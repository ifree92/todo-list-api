import { IsInt } from 'class-validator';

export class DeleteTagDto {
  @IsInt()
  id: number;
}
