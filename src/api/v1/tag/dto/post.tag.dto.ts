import { IsString, IsNotEmpty } from 'class-validator';

export class PostTagDto {
  @IsString()
  @IsNotEmpty()
  name: string;
}
