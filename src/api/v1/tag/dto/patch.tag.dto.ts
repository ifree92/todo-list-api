import { IsInt, IsString, IsNotEmpty } from 'class-validator';

export class PatchTagDto {
  @IsInt()
  id: number;

  @IsString()
  @IsNotEmpty()
  name: string;
}
