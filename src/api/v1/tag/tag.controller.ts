import {
  Controller,
  UseInterceptors,
  Get,
  Post,
  Patch,
  Delete,
  Query,
  UsePipes,
  ValidationPipe,
  Body,
} from '@nestjs/common';
import { AuthorizeUserInterceptor } from 'src/common/authorize-user.interceptor';
import { AuthorizedUser } from 'src/common/authorized-user.decorator';
import { UserEntity } from 'src/db/entities/UserEntity';
import { GetTagDto } from './dto/get.tag.dto';
import { TagService } from './tag.service';
import { PostTagDto } from './dto/post.tag.dto';
import { PatchTagDto } from './dto/patch.tag.dto';
import { DeleteTagDto } from './dto/delete.tag.dto';

@Controller('api/v1/tag')
@UseInterceptors(AuthorizeUserInterceptor)
@UsePipes(new ValidationPipe({ transform: true }))
export class TagController {
  constructor(private readonly tagService: TagService) {}

  @Get('/')
  async get(@AuthorizedUser() user: UserEntity, @Query() dto: GetTagDto) {
    return { data: await this.tagService.get(user, dto) };
  }

  @Post('/')
  async post(@AuthorizedUser() user: UserEntity, @Body() dto: PostTagDto) {
    return { data: await this.tagService.post(user, dto) };
  }

  @Patch('/')
  async patch(@Body() dto: PatchTagDto) {
    return { data: await this.tagService.patch(dto) };
  }

  @Delete('/')
  async delete(@Body() dto: DeleteTagDto) {
    await this.tagService.delete(dto);
    return { data: 'ok' };
  }
}
