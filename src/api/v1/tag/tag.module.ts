import { Module } from '@nestjs/common';
import { TagController } from './tag.controller';
import { TagService } from './tag.service';
import { DbModule } from 'src/db/db.module';
import { ConfigModule } from 'src/config/config.module';

@Module({
  controllers: [TagController],
  providers: [TagService],
  imports: [DbModule, ConfigModule],
})
export class TagModule {}
