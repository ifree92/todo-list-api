import { Injectable } from '@nestjs/common';
import { TaskTagServiceDb } from 'src/db/services/task-tag.service.db';
import { GetTagDto } from './dto/get.tag.dto';
import { UserEntity } from 'src/db/entities/UserEntity';
import { PostTagDto } from './dto/post.tag.dto';
import { PatchTagDto } from './dto/patch.tag.dto';
import { DeleteTagDto } from './dto/delete.tag.dto';

@Injectable()
export class TagService {
  constructor(private readonly taskTagService: TaskTagServiceDb) {}

  async get(user: UserEntity, dto: GetTagDto) {
    return await this.taskTagService.getAll(user, dto.skip, dto.take, dto.search);
  }

  async post(user: UserEntity, dto: PostTagDto) {
    return await this.taskTagService.create(user, dto.name);
  }

  async patch(dto: PatchTagDto) {
    return await this.taskTagService.patch(dto.id, dto.name);
  }

  async delete(dto: DeleteTagDto) {
    await this.taskTagService.delete(dto.id);
  }
}
