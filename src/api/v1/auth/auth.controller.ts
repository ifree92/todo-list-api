import { Controller, Post, Body, UsePipes, ValidationPipe } from '@nestjs/common';
import { PostSignUpDto } from './dto/post.sign-up.dto';
import { PostSignInDto } from './dto/post.sign-in.dto';
import { AuthService } from './auth.service';

@Controller('api/v1/auth')
@UsePipes(new ValidationPipe({ transform: true }))
export class AuthController {
  constructor(private readonly as: AuthService) {}

  @Post('signup')
  async signUp(@Body() dto: PostSignUpDto) {
    return {
      data: {
        token: await this.as.signUp(dto),
      },
    };
  }

  @Post('signin')
  async signIn(@Body() dto: PostSignInDto) {
    return {
      data: {
        token: await this.as.signIn(dto),
      },
    };
  }
}
