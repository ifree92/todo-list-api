import { Injectable } from '@nestjs/common';
import { PostSignUpDto } from './dto/post.sign-up.dto';
import { PostSignInDto } from './dto/post.sign-in.dto';
import { UserServiceDb } from 'src/db/services/user.service.db';
import { sign } from 'jsonwebtoken';
import { ConfigService } from 'src/config/config.service';
import { IJWTAuthPayload } from 'src/common/jwt-auth-payload.interface';

@Injectable()
export class AuthService {
  constructor(private readonly userService: UserServiceDb, private readonly cs: ConfigService) {}
  async signUp(dto: PostSignUpDto): Promise<string> {
    const user = await this.userService.create(dto);
    const payload: IJWTAuthPayload = { id: user.id };
    return sign(payload, this.cs.getSecretPhrase(), { expiresIn: '7d' });
  }

  async signIn(dto: PostSignInDto) {
    const user = await this.userService.verify(dto);
    const payload: IJWTAuthPayload = { id: user.id };
    return sign(payload, this.cs.getSecretPhrase(), { expiresIn: '7d' });
  }
}
