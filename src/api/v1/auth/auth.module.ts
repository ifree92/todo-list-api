import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { DbModule } from 'src/db/db.module';
import { ConfigModule } from 'src/config/config.module';

@Module({
  controllers: [AuthController],
  providers: [AuthService],
  imports: [DbModule, ConfigModule],
})
export class AuthModule {}
