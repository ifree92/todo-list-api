import { Module } from '@nestjs/common';
import { TaskController } from './task.controller';
import { TaskService } from './task.service';
import { ConfigModule } from 'src/config/config.module';
import { DbModule } from 'src/db/db.module';

@Module({
  controllers: [TaskController],
  providers: [TaskService],
  imports: [ConfigModule, DbModule],
})
export class TaskModule {}
