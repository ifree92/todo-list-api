import { IsInt } from 'class-validator';

export class DeleteTaskDto {
  @IsInt()
  id: number;
}
