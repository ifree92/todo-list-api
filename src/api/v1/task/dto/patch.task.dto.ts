import { IsInt, IsString, IsNotEmpty, IsOptional, IsBoolean, IsArray, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';

export class PatchTaskDto {
  @IsInt()
  id: number;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  title?: string;

  @IsString()
  @IsOptional()
  text?: string;

  @IsBoolean()
  @IsOptional()
  isCompleted?: boolean;

  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => Number)
  @IsOptional()
  tags?: Array<number>;
}
