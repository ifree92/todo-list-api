import { IsInt, IsOptional, IsString, IsBoolean, IsArray, ValidateNested, IsBooleanString } from 'class-validator';
import { Type, Transform } from 'class-transformer';

export class GetTaskDto {
  @IsInt()
  @IsOptional()
  @Type(() => Number)
  skip?: number;

  @IsInt()
  @IsOptional()
  @Type(() => Number)
  take?: number;

  @IsString()
  @IsOptional()
  search?: string;

  @IsBoolean()
  @IsOptional()
  @Transform((val, obj, type) => val.trim().toLowerCase() === 'true')
  isCompleted?: boolean;

  @IsString()
  @IsOptional()
  tags?: string;
}
