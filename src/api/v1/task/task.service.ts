import { Injectable } from '@nestjs/common';
import { TaskServiceDb } from 'src/db/services/task.service.db';
import { UserEntity } from 'src/db/entities/UserEntity';
import { CreateTaskDto } from './dto/create.task.dto';
import { GetTaskDto } from './dto/get.task.dto';
import { isNumber } from 'util';
import { DeleteTaskDto } from './dto/delete.task.dto';
import { PatchTaskDto } from './dto/patch.task.dto';

@Injectable()
export class TaskService {
  constructor(private readonly taskService: TaskServiceDb) {}

  async get(user: UserEntity, dto: GetTaskDto) {
    let tags: number[] | undefined;
    if (dto.tags) {
      tags = dto.tags
        .trim()
        .split(',')
        .map((item) => parseInt(item.trim()))
        .filter((item) => Number.isInteger(item));
    }
    return await this.taskService.getAll(user, dto.skip, dto.take, dto.search, tags, dto.isCompleted);
  }

  async post(user: UserEntity, dto: CreateTaskDto) {
    return await this.taskService.create({ user, tags: dto.tags, text: dto.text, title: dto.title });
  }

  async patch(dto: PatchTaskDto) {
    return await this.taskService.patch(dto);
  }

  async delete(dto: DeleteTaskDto) {
    await this.taskService.delete(dto.id);
  }
}
