import {
  Controller,
  Get,
  UseInterceptors,
  Post,
  Body,
  UsePipes,
  ValidationPipe,
  Query,
  Patch,
  Delete,
} from '@nestjs/common';
import { TaskService } from './task.service';
import { AuthorizeUserInterceptor } from 'src/common/authorize-user.interceptor';
import { AuthorizedUser } from 'src/common/authorized-user.decorator';
import { UserEntity } from 'src/db/entities/UserEntity';
import { CreateTaskDto } from './dto/create.task.dto';
import { GetTaskDto } from './dto/get.task.dto';
import { DeleteTaskDto } from './dto/delete.task.dto';
import { PatchTaskDto } from './dto/patch.task.dto';

@Controller('api/v1/task')
@UsePipes(new ValidationPipe({ transform: true }))
@UseInterceptors(AuthorizeUserInterceptor)
export class TaskController {
  constructor(private readonly taskService: TaskService) {}

  @Get('/')
  async getAll(@AuthorizedUser() user: UserEntity, @Query() dto: GetTaskDto) {
    console.log(dto);
    return { data: await this.taskService.get(user, dto) };
  }

  @Post('/')
  async post(@AuthorizedUser() user: UserEntity, @Body() dto: CreateTaskDto) {
    return { data: await this.taskService.post(user, dto) };
  }

  @Patch('/')
  async patch(@Body() dto: PatchTaskDto) {
    return { data: await this.taskService.patch(dto) };
  }

  @Delete('/')
  async delete(@Body() dto: DeleteTaskDto) {
    await this.taskService.delete(dto);
    return { data: 'ok' };
  }
}
