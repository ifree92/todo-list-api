import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from './config/config.service';

@Injectable()
export class AppService {
  private readonly log = new Logger(AppService.name, true);

  constructor(private readonly cs: ConfigService) {}

  async preStart() {
    this.log.log('preStart');
  }

  async postStart() {
    this.log.log('postStart');
  }
}
