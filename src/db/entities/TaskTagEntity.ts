import { Entity, PrimaryGeneratedColumn, Column, ManyToMany, ManyToOne, JoinTable } from 'typeorm';
import { TaskEntity } from './TaskEntity';
import { UserEntity } from './UserEntity';

@Entity()
export class TaskTagEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @ManyToMany((type) => TaskEntity)
  @JoinTable()
  tasks: Array<TaskEntity>;

  @ManyToOne(
    (type) => UserEntity,
    (userEntity) => userEntity.tags,
  )
  user: UserEntity;
}
