import { Entity, PrimaryGeneratedColumn, ManyToOne, ManyToMany, JoinTable, Column } from 'typeorm';
import { UserEntity } from './UserEntity';
import { TaskTagEntity } from './TaskTagEntity';

@Entity()
export class TaskEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false })
  title: string;

  @Column({ nullable: false })
  text: string;

  @Column({ nullable: false, default: false })
  isCompleted: boolean = false;

  @Column({ nullable: false })
  createdAt: Date;

  @Column({ nullable: false })
  updatedAt: Date;

  @ManyToOne(
    () => UserEntity,
    (userEntity) => userEntity.tasks,
  )
  user: UserEntity;

  @ManyToMany(
    (type) => TaskTagEntity,
    (taskTagEntity) => taskTagEntity.tasks,
  )
  @JoinTable()
  tags: Array<TaskTagEntity>;
}
