import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { TaskEntity } from './TaskEntity';
import { TaskTagEntity } from './TaskTagEntity';

@Entity()
export class UserEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false, unique: true })
  login: string;

  @Column({ nullable: false })
  password: string;

  @OneToMany(
    () => TaskEntity,
    (taskEntity) => taskEntity.user,
  )
  tasks: Array<TaskEntity>;

  @OneToMany(
    () => TaskTagEntity,
    (taskTagEntity) => taskTagEntity.user,
  )
  tags: Array<TaskTagEntity>;
}
