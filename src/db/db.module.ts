import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from 'src/config/config.module';
import { ConfigService } from 'src/config/config.service';
import { UserServiceDb } from './services/user.service.db';
import { TaskServiceDb } from './services/task.service.db';
import { TaskTagServiceDb } from './services/task-tag.service.db';
import { UserRepository } from './repositories/UserRepository';
import { TaskRepository } from './repositories/TaskRepository';
import { TaskTagRepository } from './repositories/TaskTagRepository';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (cs: ConfigService) => {
        return {
          type: 'sqlite',
          database: cs.getDbFile(),
          entities: [__dirname + '/entities/*{.ts,.js}'],
          migrations: [__dirname + '/migrations/**/*{.ts,.js}'],
          synchronize: false,
          migrationsRun: true,
          logging: cs.getIsDev(),
          cli: {
            migrationsDir: 'src/migrations',
          },
        };
      },
    }),
    TypeOrmModule.forFeature([UserRepository, TaskRepository, TaskTagRepository]),
  ],
  providers: [UserServiceDb, TaskServiceDb, TaskTagServiceDb],
  exports: [UserServiceDb, TaskServiceDb, TaskTagServiceDb],
})
export class DbModule {}
