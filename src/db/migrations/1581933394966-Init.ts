import {MigrationInterface, QueryRunner} from "typeorm";

export class Init1581933394966 implements MigrationInterface {
    name = 'Init1581933394966'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "task_tag_entity" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar NOT NULL, "userId" integer)`, undefined);
        await queryRunner.query(`CREATE TABLE "user_entity" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "login" varchar NOT NULL, "password" varchar NOT NULL, CONSTRAINT "UQ_e74b542753b5bf00d728607f81a" UNIQUE ("login"))`, undefined);
        await queryRunner.query(`CREATE TABLE "task_entity" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "title" varchar NOT NULL, "text" varchar NOT NULL, "isCompleted" boolean NOT NULL DEFAULT (0), "createdAt" datetime NOT NULL, "updatedAt" datetime NOT NULL, "userId" integer)`, undefined);
        await queryRunner.query(`CREATE TABLE "task_entity_tags_task_tag_entity" ("taskEntityId" integer NOT NULL, "taskTagEntityId" integer NOT NULL, PRIMARY KEY ("taskEntityId", "taskTagEntityId"))`, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_15865e7b14289eb2e3c3c6b238" ON "task_entity_tags_task_tag_entity" ("taskEntityId") `, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_40b6a79998941466190eb281f3" ON "task_entity_tags_task_tag_entity" ("taskTagEntityId") `, undefined);
        await queryRunner.query(`CREATE TABLE "temporary_task_tag_entity" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar NOT NULL, "userId" integer, CONSTRAINT "FK_76ebefb20d51f8301135d836b30" FOREIGN KEY ("userId") REFERENCES "user_entity" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION)`, undefined);
        await queryRunner.query(`INSERT INTO "temporary_task_tag_entity"("id", "name", "userId") SELECT "id", "name", "userId" FROM "task_tag_entity"`, undefined);
        await queryRunner.query(`DROP TABLE "task_tag_entity"`, undefined);
        await queryRunner.query(`ALTER TABLE "temporary_task_tag_entity" RENAME TO "task_tag_entity"`, undefined);
        await queryRunner.query(`CREATE TABLE "temporary_task_entity" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "title" varchar NOT NULL, "text" varchar NOT NULL, "isCompleted" boolean NOT NULL DEFAULT (0), "createdAt" datetime NOT NULL, "updatedAt" datetime NOT NULL, "userId" integer, CONSTRAINT "FK_2621bebd84d2624da37a34797fc" FOREIGN KEY ("userId") REFERENCES "user_entity" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION)`, undefined);
        await queryRunner.query(`INSERT INTO "temporary_task_entity"("id", "title", "text", "isCompleted", "createdAt", "updatedAt", "userId") SELECT "id", "title", "text", "isCompleted", "createdAt", "updatedAt", "userId" FROM "task_entity"`, undefined);
        await queryRunner.query(`DROP TABLE "task_entity"`, undefined);
        await queryRunner.query(`ALTER TABLE "temporary_task_entity" RENAME TO "task_entity"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_15865e7b14289eb2e3c3c6b238"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_40b6a79998941466190eb281f3"`, undefined);
        await queryRunner.query(`CREATE TABLE "temporary_task_entity_tags_task_tag_entity" ("taskEntityId" integer NOT NULL, "taskTagEntityId" integer NOT NULL, CONSTRAINT "FK_15865e7b14289eb2e3c3c6b2389" FOREIGN KEY ("taskEntityId") REFERENCES "task_entity" ("id") ON DELETE CASCADE ON UPDATE NO ACTION, CONSTRAINT "FK_40b6a79998941466190eb281f3a" FOREIGN KEY ("taskTagEntityId") REFERENCES "task_tag_entity" ("id") ON DELETE CASCADE ON UPDATE NO ACTION, PRIMARY KEY ("taskEntityId", "taskTagEntityId"))`, undefined);
        await queryRunner.query(`INSERT INTO "temporary_task_entity_tags_task_tag_entity"("taskEntityId", "taskTagEntityId") SELECT "taskEntityId", "taskTagEntityId" FROM "task_entity_tags_task_tag_entity"`, undefined);
        await queryRunner.query(`DROP TABLE "task_entity_tags_task_tag_entity"`, undefined);
        await queryRunner.query(`ALTER TABLE "temporary_task_entity_tags_task_tag_entity" RENAME TO "task_entity_tags_task_tag_entity"`, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_15865e7b14289eb2e3c3c6b238" ON "task_entity_tags_task_tag_entity" ("taskEntityId") `, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_40b6a79998941466190eb281f3" ON "task_entity_tags_task_tag_entity" ("taskTagEntityId") `, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DROP INDEX "IDX_40b6a79998941466190eb281f3"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_15865e7b14289eb2e3c3c6b238"`, undefined);
        await queryRunner.query(`ALTER TABLE "task_entity_tags_task_tag_entity" RENAME TO "temporary_task_entity_tags_task_tag_entity"`, undefined);
        await queryRunner.query(`CREATE TABLE "task_entity_tags_task_tag_entity" ("taskEntityId" integer NOT NULL, "taskTagEntityId" integer NOT NULL, PRIMARY KEY ("taskEntityId", "taskTagEntityId"))`, undefined);
        await queryRunner.query(`INSERT INTO "task_entity_tags_task_tag_entity"("taskEntityId", "taskTagEntityId") SELECT "taskEntityId", "taskTagEntityId" FROM "temporary_task_entity_tags_task_tag_entity"`, undefined);
        await queryRunner.query(`DROP TABLE "temporary_task_entity_tags_task_tag_entity"`, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_40b6a79998941466190eb281f3" ON "task_entity_tags_task_tag_entity" ("taskTagEntityId") `, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_15865e7b14289eb2e3c3c6b238" ON "task_entity_tags_task_tag_entity" ("taskEntityId") `, undefined);
        await queryRunner.query(`ALTER TABLE "task_entity" RENAME TO "temporary_task_entity"`, undefined);
        await queryRunner.query(`CREATE TABLE "task_entity" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "title" varchar NOT NULL, "text" varchar NOT NULL, "isCompleted" boolean NOT NULL DEFAULT (0), "createdAt" datetime NOT NULL, "updatedAt" datetime NOT NULL, "userId" integer)`, undefined);
        await queryRunner.query(`INSERT INTO "task_entity"("id", "title", "text", "isCompleted", "createdAt", "updatedAt", "userId") SELECT "id", "title", "text", "isCompleted", "createdAt", "updatedAt", "userId" FROM "temporary_task_entity"`, undefined);
        await queryRunner.query(`DROP TABLE "temporary_task_entity"`, undefined);
        await queryRunner.query(`ALTER TABLE "task_tag_entity" RENAME TO "temporary_task_tag_entity"`, undefined);
        await queryRunner.query(`CREATE TABLE "task_tag_entity" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar NOT NULL, "userId" integer)`, undefined);
        await queryRunner.query(`INSERT INTO "task_tag_entity"("id", "name", "userId") SELECT "id", "name", "userId" FROM "temporary_task_tag_entity"`, undefined);
        await queryRunner.query(`DROP TABLE "temporary_task_tag_entity"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_40b6a79998941466190eb281f3"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_15865e7b14289eb2e3c3c6b238"`, undefined);
        await queryRunner.query(`DROP TABLE "task_entity_tags_task_tag_entity"`, undefined);
        await queryRunner.query(`DROP TABLE "task_entity"`, undefined);
        await queryRunner.query(`DROP TABLE "user_entity"`, undefined);
        await queryRunner.query(`DROP TABLE "task_tag_entity"`, undefined);
    }

}
