import { Injectable } from '@nestjs/common';
import { TaskEntity } from '../entities/TaskEntity';
import { TaskRepository } from '../repositories/TaskRepository';
import { ICreateTaskDto } from './dto/create.task.interface';
import { UserEntity } from '../entities/UserEntity';
import { TaskTagRepository } from '../repositories/TaskTagRepository';
import { In, Like } from 'typeorm';
import { IPatchTaskDto } from './dto/patch.task.interface';
import { TaskTagEntity } from '../entities/TaskTagEntity';

@Injectable()
export class TaskServiceDb {
  constructor(private readonly taskRepository: TaskRepository, private readonly taskTagRepository: TaskTagRepository) {}

  async create(dto: ICreateTaskDto) {
    const tags = await this.taskTagRepository.find({ where: { id: In(dto.tags) } });
    const task = new TaskEntity();
    task.createdAt = new Date();
    task.updatedAt = new Date();
    task.isCompleted = false;
    task.text = dto.text;
    task.title = dto.title;
    task.user = dto.user;
    task.tags = tags;
    await this.taskRepository.save(task);
    task.user = undefined;
    return task;
  }

  async getAll(user: UserEntity, skip = 0, take = 10, search = '', tags?: number[], isCompleted?: boolean) {
    let tasks = await this.taskRepository.smartFind({
      userId: user.id,
      skip,
      take,
      search,
      tags: tags ?? [],
      isCompleted,
    });
    for (const task of tasks) {
      const tags = await this.taskTagRepository.getTagsByTaskId(task.id, task.user.id);
      task.tags = tags.map((tag) => ({ ...tag, tasks: undefined, user: undefined }));
    }
    tasks = tasks.map((task) => ({ ...task, user: undefined }));
    return tasks;
  }

  async delete(taskId: number) {
    await this.taskRepository.delete({ id: taskId });
  }

  async patch(data: IPatchTaskDto) {
    const task = await this.taskRepository.findOne({ where: { id: data.id }, relations: ['tags'] });
    if (data.isCompleted !== undefined) task.isCompleted = data.isCompleted;
    if (data.text !== undefined) task.text = data.text;
    if (data.title !== undefined) task.title = data.title;
    if (data.tags !== undefined) {
      if (data.tags.length === 0) task.tags = [];
      else task.tags = await this.taskTagRepository.find({ where: { id: In(data.tags) } });
    }
    await this.taskRepository.save(task);
    return task;
  }
}
