import { Injectable, NotFoundException } from '@nestjs/common';
import { UserEntity } from '../entities/UserEntity';
import { TaskTagRepository } from '../repositories/TaskTagRepository';
import { TaskTagEntity } from '../entities/TaskTagEntity';
import { Like } from 'typeorm';

@Injectable()
export class TaskTagServiceDb {
  constructor(private readonly taskTagRepository: TaskTagRepository) {}

  async create(user: UserEntity, name: string): Promise<TaskTagEntity> {
    const tag = new TaskTagEntity();
    tag.name = name;
    tag.user = user;
    await this.taskTagRepository.save(tag);
    tag.user = undefined;
    return tag;
  }

  async getAll(user: UserEntity, skip = 0, take = 10, search = '') {
    return await this.taskTagRepository.find({ where: { user, name: Like(`%${search}%`) }, skip, take });
  }

  async patch(id: number, name: string) {
    const tag = await this.taskTagRepository.findOne({ id });
    if (!tag) throw new NotFoundException(`Tag with id ${id} doesn't exist`);
    tag.name = name;
    await this.taskTagRepository.save(tag);
    return tag;
  }

  async delete(id: number) {
    await this.taskTagRepository.delete({ id });
  }
}
