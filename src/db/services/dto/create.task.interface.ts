import { UserEntity } from 'src/db/entities/UserEntity';
import { TaskTagEntity } from 'src/db/entities/TaskTagEntity';

export interface ICreateTaskDto {
  user: UserEntity;
  tags: Array<number>;
  text: string;
  title: string;
}
