export interface IPatchTaskDto {
  id: number;
  title?: string;
  text?: string;
  isCompleted?: boolean;
  tags?: Array<number>;
}
