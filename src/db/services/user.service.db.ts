import { Injectable, NotFoundException, ForbiddenException } from '@nestjs/common';
import { ICreateVerifyUserDto } from './dto/create.verify.user.interface';
import { UserEntity } from '../entities/UserEntity';
import { createHash } from 'crypto';
import { UserRepository } from '../repositories/UserRepository';

@Injectable()
export class UserServiceDb {
  constructor(private readonly userRepository: UserRepository) {}

  async create(dto: ICreateVerifyUserDto): Promise<UserEntity> {
    const hashedPassword = createHash('md5')
      .update(dto.password)
      .digest('hex');
    const user = new UserEntity();
    user.login = dto.login;
    user.password = hashedPassword;
    await this.userRepository.save(user);
    return user;
  }

  async verify(dto: ICreateVerifyUserDto): Promise<UserEntity> {
    const hashedPassword = createHash('md5')
      .update(dto.password)
      .digest('hex');
    const user = await this.userRepository.findOne({ login: dto.login });
    if (!user) {
      throw new NotFoundException(`User ${dto.login} doesn't exist`);
    }
    console.log(user);
    if (user.password !== hashedPassword) {
      throw new ForbiddenException(`Bad password for user ${dto.login}`);
    }
    return user;
  }

  async getById(id: number): Promise<UserEntity | undefined> {
    return await this.userRepository.findOne({ id });
  }
}
