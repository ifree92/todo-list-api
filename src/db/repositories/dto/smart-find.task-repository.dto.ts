export interface ISmartFindTaskRepositoryDto {
  userId: number;
  take: number;
  skip: number;
  search: string;
  tags: number[];
  isCompleted?: boolean;
}
