import { Repository, EntityRepository } from 'typeorm';
import { TaskEntity } from '../entities/TaskEntity';
import { ISmartFindTaskRepositoryDto } from './dto/smart-find.task-repository.dto';

@EntityRepository(TaskEntity)
export class TaskRepository extends Repository<TaskEntity> {
  smartFind(data: ISmartFindTaskRepositoryDto) {
    const q = this.createQueryBuilder('te');
    q.leftJoinAndSelect('te.tags', 'tags');
    q.leftJoinAndSelect('te.user', 'user');
    q.where('user.id = :id', { id: data.userId });
    if (data.search) {
      q.andWhere('te.title LIKE :title', { title: `%${data.search}%` });
    }
    if (data.tags.length) {
      q.andWhere('tags.id IN (:...ids)', { ids: data.tags });
    }
    if (data.isCompleted !== undefined) {
      q.andWhere('te.isCompleted = :isCompleted', { isCompleted: data.isCompleted });
    }
    q.limit(data.take);
    q.offset(data.skip);
    return q.getMany();
  }
}
