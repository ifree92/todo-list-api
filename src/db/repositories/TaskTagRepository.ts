import { Repository, EntityRepository } from 'typeorm';
import { TaskTagEntity } from '../entities/TaskTagEntity';

@EntityRepository(TaskTagEntity)
export class TaskTagRepository extends Repository<TaskTagEntity> {
  async getTagsByTaskId(taskId: number, userId: number) {
    return this.createQueryBuilder('tte')
      .leftJoinAndSelect('tte.user', 'user')
      .leftJoinAndSelect('tte.tasks', 'tasks')
      .where('user.id = :id', { id: userId })
      .where('tasks.id = :taskId', { taskId })
      .getMany();
  }
}
