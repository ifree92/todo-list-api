import { Injectable } from '@nestjs/common';
import { config } from 'dotenv';

@Injectable()
export class ConfigService {
  constructor() {
    config();
  }

  getPort(): number {
    return parseInt(process.env.PORT);
  }

  getDbFile(): string {
    return process.env.DB_FILE;
  }

  getSecretPhrase(): string {
    return process.env.SECRET_PHRASE;
  }

  getIsDev(): boolean {
    return !!process.env.DEV;
  }
}
