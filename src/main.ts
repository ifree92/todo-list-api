import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { NestFastifyApplication, FastifyAdapter } from '@nestjs/platform-fastify';
import { ConfigService } from './config/config.service';
import { Logger } from '@nestjs/common';
import { AppService } from './app.service';

async function bootstrap() {
  const app = await NestFactory.create<NestFastifyApplication>(AppModule, new FastifyAdapter());
  const log = new Logger('bootstrap');
  const configService = app.get(ConfigService);
  const appService = app.get(AppService);
  await appService.preStart();
  await app.listen(configService.getPort(), '0.0.0.0');
  if (configService.getIsDev()) {
    app.enableCors();
    log.log('CORS enabled');
  }
  log.log(`Listening 0.0.0.0:${configService.getPort()}`);
  await appService.postStart();
}
bootstrap();
