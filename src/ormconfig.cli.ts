import { ConnectionOptions } from 'typeorm';
import * as dotenv from 'dotenv';

dotenv.config();

const ormConfigCli: ConnectionOptions = {
  type: 'sqlite',
  database: process.env.DB_FILE,
  entities: [__dirname + '/db/entities/*{.ts,.js}'],
  migrations: [__dirname + '/db/migrations/**/*{.ts,.js}'],
  synchronize: false,
  migrationsRun: true,
  cli: {
    migrationsDir: 'src/db/migrations',
  },
};

export = ormConfigCli;
