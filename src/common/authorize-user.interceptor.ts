import { CallHandler, ExecutionContext, Injectable, NestInterceptor, UnauthorizedException } from '@nestjs/common';
import { Observable } from 'rxjs';
import { FastifyRequest } from 'fastify';
import { ConfigService } from 'src/config/config.service';
import { verify } from 'jsonwebtoken';
import { IJWTAuthPayload } from './jwt-auth-payload.interface';
import { UserServiceDb } from 'src/db/services/user.service.db';

@Injectable()
export class AuthorizeUserInterceptor implements NestInterceptor {
  constructor(private readonly cs: ConfigService, private readonly userService: UserServiceDb) {}

  async intercept(context: ExecutionContext, next: CallHandler): Promise<Observable<any>> {
    const req: FastifyRequest = context.switchToHttp().getRequest();
    const authToken = req.headers['auth-token'];
    if (!authToken) throw new UnauthorizedException('auth-token header not found');
    let payload: IJWTAuthPayload;
    try {
      payload = verify(authToken, this.cs.getSecretPhrase()) as IJWTAuthPayload;
    } catch (e) {
      throw new UnauthorizedException(`Bad token: ${e.message}`);
    }
    const user = await this.userService.getById(payload.id);
    if (!user) throw new UnauthorizedException('Unknown user');
    // @ts-ignore
    req.userEntity = user;
    return next.handle();
  }
}
