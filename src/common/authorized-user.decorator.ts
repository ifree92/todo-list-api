import { SetMetadata, createParamDecorator } from '@nestjs/common';

export const AuthorizedUser = createParamDecorator((data, req) => {
  return req.userEntity;
});
