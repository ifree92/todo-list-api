import { Module } from '@nestjs/common';
import { AppService } from './app.service';
import { ConfigModule } from './config/config.module';
import { DbModule } from './db/db.module';
import { AuthModule } from './api/v1/auth/auth.module';
import { TaskModule } from './api/v1/task/task.module';
import { CommonModule } from './common/common.module';
import { TagModule } from './api/v1/tag/tag.module';

@Module({
  imports: [ConfigModule, DbModule, AuthModule, TaskModule, CommonModule, TagModule],
  providers: [AppService],
})
export class AppModule {}
